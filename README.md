# Blixt Circuit Breaker Controller

## Introduction
Blixt Circuit Breaker Controller (``bcbcontroller-gui``) is a graphical user interface application for controlling the Blixt Circuit Breaker its USB-Serial interface.

## Prerequisites
To connect the circuit breaker with ``bcbcontroller-gui``, you need to have ``BCBSerialController`` application running on the circuit breaker. 

The following **Arduino** code snippet shows how to initialise and run ``BCBSerialController`` application on the circuit breaker. 

```cpp
#include <Arduino.h>
#include <BCBSerialController.h>
#include <BCB.h>

void setup() {
  Serial.begin(115200);
  BCBSerialController::init();
  BCB::init();
}

void loop() {
  yield();
}
```

## Setting up development environment
As ``bcbcontroller-gui`` is written in Python, we use ``conda`` package manager to setup the development environment.

* Install ``minoconda`` which is a minimalist version of  ``conda`` as mentioned [here](https://docs.conda.io/en/latest/miniconda.html).
* Open a terninal and create a virtual Python environment named ``bcbcontroller`` as follows.
	* ``conda create -n bcbcontroller python=3`` 
* Activate the newly created enviroment as follows.
	* ``conda activate bcbcontroller`` 
	
* Install needed packages using ``pip``
	* ``pip install msgpack numpy pyqt5 pyqtgraph pyserial``
	
## Starting the controller application

Inside the ``bcbcontroller`` environment, use the following command to start ``bcbcontroller-gui``.

``(bcbcontroller) $ python bcb_controller.py`` 
 
 