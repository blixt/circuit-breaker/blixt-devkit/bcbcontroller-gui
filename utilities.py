def get_config(config, default, *keys):
    for key in keys:
        try:
            config = config[key]
        except KeyError:
            return default
    return config