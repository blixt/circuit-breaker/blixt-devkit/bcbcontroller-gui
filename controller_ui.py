import sys
import pyqtgraph
import numpy
import PyQt5
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets
import multiprocessing
import signal
import random
import time
import logging
import serialio
import mainwindow
import dataframe
import dataview
import bcb
import utilities


class ControllerUI(QtWidgets.QWidget):

    def __init__(self, config):
        super().__init__()
        self.logger = logging.getLogger(self.__class__.__name__)
        self.config = config

        self.bcb_status = bcb.BCBStatus()
        self.bcb_if = bcb.BCBInterface()
        self.bcb_if.set_callback('connected', self._on_bcb_connected)
        self.bcb_if.set_callback('disconnected', self._on_bcb_disconnected)
        self.bcb_if.set_callback('measurement', self._on_bcb_measurement)
        self.bcb_if.set_callback('status', self._on_bcb_status)
        self.bcb_if.set_callback('debug', self._on_bcb_debug_text)
        self.bcb_if.set_callback('error', self._on_bcb_error)

        self.main_window_ui = mainwindow.Ui_mainWindow()
        self.main_window_ui.setupUi(self)

        self.main_window_ui.baudrateComboBox.addItem("1200", 1200)
        self.main_window_ui.baudrateComboBox.addItem("4800", 4800)
        self.main_window_ui.baudrateComboBox.addItem("9600", 9600)
        self.main_window_ui.baudrateComboBox.addItem("19200", 19200)
        self.main_window_ui.baudrateComboBox.addItem("38400", 38400)
        self.main_window_ui.baudrateComboBox.addItem("57600", 57600)
        self.main_window_ui.baudrateComboBox.addItem("115200", 115200)
        self.main_window_ui.baudrateComboBox.setCurrentIndex(
            self.main_window_ui.baudrateComboBox.findText("115200"))

        self.plot_layout = QtGui.QVBoxLayout(self.main_window_ui.plotFrame)
        plot_background_color = utilities.get_config(self.config, "#FFFFFF", "dataviews",
                                                     "background_color")
        pyqtgraph.setConfigOption('background', plot_background_color)

        self.iv_data_view = dataview.IVDataView(self.config)
        for widget in self.iv_data_view.get_widgets():
            self.plot_layout.addWidget(widget)

        self.adc_data = {}
        self.adc_data_live_view = {}

        self.data_timer = QtCore.QTimer()
        self.status_timer = QtCore.QTimer()
        self.dev_lookup_timer = QtCore.QTimer()

        self.main_window_ui.onButton.clicked.connect(self._on_button_clicked)
        self.main_window_ui.ocpotpResetButton.clicked.connect(self._on_ocp_otp_button_clicked)
        self.main_window_ui.calibrateButton.clicked.connect(self._on_calibrate_button_clicked)
        self.main_window_ui.mcuResetButton.clicked.connect(self._on_mcu_reset_button_clicked)
        self.main_window_ui.startButton.clicked.connect(self._on_start_button_clicked)
        self.main_window_ui.realtimeCheckBox.stateChanged.connect(
            self._on_real_time_checkbox_changed)
        self.main_window_ui.debugInputLineEdit.installEventFilter(self)
        self.main_window_ui.clearDebugTxtButton.clicked.connect(self._on_clear_debug_text_clicked)
        self.main_window_ui.connectButton.clicked.connect(self._on_connect_button_clicked)
        self.main_window_ui.autoConnectCheckBox.stateChanged.connect(
            self._on_auto_connect_checkbox_changed)
        self.main_window_ui.scanPortsButton.clicked.connect(self._on_scan_ports_button_clicked)

        self.data_timer.timeout.connect(self._on_data_timer)
        self.status_timer.timeout.connect(self._on_status_timer)
        self.dev_lookup_timer.timeout.connect(self._on_dev_lookup_timer)

        self._on_real_time_checkbox_changed()

    ###################################### Signal handlers #########################################
    def _on_button_clicked(self):
        if self.bcb_status.is_on:
            self.bcb_if.off()
        else:
            self.bcb_if.on()
        self.bcb_if.get_status()
        self.main_window_ui.onButton.setEnabled(False)

    def _on_ocp_otp_button_clicked(self):
        if self.bcb_status.is_ocp_triggered or self.bcb_status.is_ocp_triggered:
            self.bcb_if.ocp_otp_reset()

    def _on_mcu_reset_button_clicked(self):
        self.main_window_ui.mcuResetButton.setEnabled(False)
        self.bcb_if.reset()
        self.main_window_ui.mcuResetButton.setEnabled(True)

    def _on_calibrate_button_clicked(self):
        self.bcb_if.calibrate()

    def _on_start_button_clicked(self):
        if not self.bcb_status.is_adc_dump_active:
            self._start_adc_acquisition()
        else:
            self._stop_adc_acquisition()

    def _on_real_time_checkbox_changed(self):
        if self.main_window_ui.realtimeCheckBox.isChecked():
            self.main_window_ui.acquisitionProgressBar.setEnabled(False)
            self.iv_data_view.enable_fvs(False)
            self.main_window_ui.acquisitionProgressBar.setProperty("value", 0)
        else:
            self.main_window_ui.acquisitionProgressBar.setEnabled(True)
            self.iv_data_view.enable_fvs(True)

    def eventFilter(self, source, event):
        if (event.type() == QtCore.QEvent.KeyPress and source is self.main_window_ui.debugInputLineEdit):
            if event.key() == QtCore.Qt.Key_Return:
                text = source.text().strip()
                if len(text) > 0:
                    self.main_window_ui.debugInputLineEdit.setText("")

        return super(QtWidgets.QWidget, self).eventFilter(source, event)

    def _on_clear_debug_text_clicked(self):
        self.main_window_ui.debugOutputTextEdit.clear()

    def _on_connect_button_clicked(self):
        if self.bcb_if.is_connected():
            self._disconnect_bcb()
        else:
            self._connect_bcb()

    def _on_auto_connect_checkbox_changed(self):
        if self.main_window_ui.autoConnectCheckBox.isChecked():
            self.main_window_ui.connectButton.setEnabled(False)
            self.main_window_ui.portsComboBox.setEnabled(False)
            self.main_window_ui.baudrateComboBox.setEnabled(False)
            self.main_window_ui.scanPortsButton.setEnabled(False)
            self.dev_lookup_timer.start(1000)
        else:
            self.dev_lookup_timer.stop()
            self.main_window_ui.connectButton.setEnabled(True)
            self.main_window_ui.portsComboBox.setEnabled(True)
            self.main_window_ui.baudrateComboBox.setEnabled(True)
            self.main_window_ui.scanPortsButton.setEnabled(True)

    def _on_scan_ports_button_clicked(self):
        dev_list = bcb.BCBInterface.get_devices()
        self.main_window_ui.portsComboBox.clear()
        self.main_window_ui.portsComboBox.addItems(dev_list)

    ################################### Local funtions #############################################
    def _connect_bcb(self):
        self.config['port'] = self.main_window_ui.portsComboBox.currentText()
        self.config['baudrate'] = int(self.main_window_ui.baudrateComboBox.currentText())
        if self.config.get('port') == "":
            self._show_error_text("No serial port selected")
            return
        self.bcb_if.connect(port=self.config.get('port'), baudrate=self.config.get('baudrate'))

    def _disconnect_bcb(self):
        self._disable_ui_functions()
        self.bcb_if.disconnect()

    def _start_adc_acquisition(self):
        self.main_window_ui.startButton.setText("Stop")
        self.iv_data_view.clear()
        self.main_window_ui.acquisitionProgressBar.setProperty("value", 0)
        self.bcb_if.get_config()
        self.bcb_if.start_sampling()
        self.bcb_if.get_status()
        self._on_real_time_checkbox_changed()

    def _stop_adc_acquisition(self):
        self.main_window_ui.startButton.setText("Start")
        self.bcb_if.stop_sampling()
        self.bcb_if.get_status()

    def _update_progress(self):
        if not self.main_window_ui.realtimeCheckBox.isChecked():
            size, occupied = self.iv_data_view.get_buf_status_fv()
            percentage = occupied * 100 / size
            self.main_window_ui.acquisitionProgressBar.setProperty("value", percentage)
            if percentage == 100:
                self._stop_adc_acquisition()
        else:
            self.main_window_ui.acquisitionProgressBar.setProperty("value", 0)

    def _show_hb_status(self, received=False):
        label = self.main_window_ui.hbStatusLabel
        if received:
            label.setProperty("onHB", True)
        else:
            label.setProperty("onHB", False)
        label.style().unpolish(label)
        label.style().polish(label)

    def _show_ocp_status(self, received=False):
        label = self.main_window_ui.ocpStatusLabel
        if received:
            label.setProperty("onOCP", True)
        else:
            label.setProperty("onOCP", False)
        label.style().unpolish(label)
        label.style().polish(label)

    def _show_otp_status(self, received=False):
        label = self.main_window_ui.otpStatusLabel
        if received:
            label.setProperty("onOTP", True)
        else:
            label.setProperty("onOTP", False)
        label.style().unpolish(label)
        label.style().polish(label)

    def _show_on_status(self, is_on=False):
        on_button = self.main_window_ui.onButton
        if is_on:
            on_button.setText("Off")
            on_button.setProperty("on", True)
        else:
            on_button.setText("On")
            on_button.setProperty("on", False)
        on_button.setEnabled(True)
        on_button.style().unpolish(on_button)
        on_button.style().polish(on_button)

    def _show_sampling_status(self, is_active=False):
        if is_active:
            self.main_window_ui.startButton.setText("Stop")
        else:
            self.main_window_ui.startButton.setText("Start")

    def _show_connection_button_status(self):
        if self.bcb_if.is_connected():
            self.main_window_ui.connectButton.setText("Disconnect")
        else:
            self.main_window_ui.connectButton.setText("Connect")

    def _show_error_text(self, err_str):
        errDescLabel = self.main_window_ui.errDescLabel
        metrics = QtGui.QFontMetrics(errDescLabel.font())
        elided_err_str = metrics.elidedText(err_str, QtCore.Qt.ElideRight, errDescLabel.width())
        self.main_window_ui.errDescLabel.setText(elided_err_str)
        self.main_window_ui.errDescLabel.setToolTip(err_str)

    def _disable_ui_functions(self):
        self.data_timer.stop()
        self.status_timer.stop()
        self.main_window_ui.acqCtrlGroupBox.setEnabled(False)
        self.main_window_ui.swCtrlGroupBox.setEnabled(False)
        self.main_window_ui.debugInputLineEdit.setEnabled(False)
        self.main_window_ui.advCtrlGroupBox.setEnabled(False)
        self._show_hb_status(False)
        self._show_otp_status(False)
        self._show_ocp_status(False)
        self._show_on_status(False)
        self._show_sampling_status(False)

    def _enable_ui_functions(self):
        self.main_window_ui.acqCtrlGroupBox.setEnabled(True)
        self.main_window_ui.swCtrlGroupBox.setEnabled(True)
        self.main_window_ui.debugInputLineEdit.setEnabled(True)
        self.main_window_ui.advCtrlGroupBox.setEnabled(True)
        self.main_window_ui.errDescLabel.setText("")
        self.data_timer.start(25)
        self.status_timer.start(500)

    ################################## BCBInterface callbacks ######################################
    def _on_bcb_connected(self):
        self._enable_ui_functions()
        self._show_connection_button_status()

    def _on_bcb_disconnected(self):
        self._disable_ui_functions()
        self._show_connection_button_status()

    def _on_bcb_status(self, bcb_status):
        if bcb_status.is_on:
            self._show_on_status(True)
        else:
            self._show_on_status(False)
        self._show_hb_status(True)

        if bcb_status.is_ocp_triggered:
            self._show_ocp_status(True)
        else:
            self._show_ocp_status(False)

        if bcb_status.is_otp_triggered:
            self._show_otp_status(True)
        else:
            self._show_otp_status(False)

        if bcb_status.is_ocp_triggered or bcb_status.is_ocp_triggered:
            self.main_window_ui.ocpotpResetButton.setEnabled(True)
        else:
            self.main_window_ui.ocpotpResetButton.setEnabled(False)

        if bcb_status.is_adc_dump_active:
            self._show_sampling_status(True)
        else:
            self._show_sampling_status(False)

        if self.bcb_status.is_adc_dump_active and not bcb_status.is_adc_dump_active:
            if not self.main_window_ui.realtimeCheckBox.isChecked():
                self.iv_data_view.update_fvs()

        self.bcb_status = bcb_status

    def _on_bcb_measurement(self, time_tick, current, voltage):
        if self.bcb_status.is_adc_dump_active:
            self.iv_data_view.add_data(time_tick, current, voltage)

    def _on_bcb_debug_text(self, debug_text):
        cursor = self.main_window_ui.debugOutputTextEdit.textCursor()
        sb = self.main_window_ui.debugOutputTextEdit.verticalScrollBar()
        save_scroll = sb.value()
        save_max = (save_scroll == sb.maximum())
        self.main_window_ui.debugOutputTextEdit.moveCursor(QtGui.QTextCursor.End)
        cursor.beginEditBlock()
        cursor.insertText(debug_text)
        cursor.endEditBlock()

        if save_max:
            sb.setValue(sb.maximum())
        else:
            sb.setValue(save_scroll)

    def _on_bcb_error(self, e):
        self._show_error_text("BCB Error: " + str(e))
        self.bcb_if.disconnect()
        self._show_connection_button_status()
        self._disable_ui_functions()
        self.main_window_ui.portsComboBox.clear()

    ############################# UI functions #####################################################

    def _on_data_timer(self):
        self.bcb_if.poll()
        if self.bcb_status.is_adc_dump_active:
            self._update_progress()
            self.iv_data_view.update_lvs()

    def _on_status_timer(self):
        self._show_hb_status(False)
        self.bcb_if.get_status()

    def _on_dev_lookup_timer(self):
        dev_list = bcb.BCBInterface.get_devices()
        if self.main_window_ui.autoConnectCheckBox.isChecked():
            if len(dev_list) > 0 and not self.bcb_if.is_connected():
                self.main_window_ui.portsComboBox.clear()
                self.main_window_ui.portsComboBox.addItems(dev_list)
                self.main_window_ui.portsComboBox.setCurrentText(dev_list[0])
                self._connect_bcb()

    def show(self):
        if self.config.get('advanced'):
            self.main_window_ui.advCtrlGroupBox.setVisible(True)
        else:
            self.main_window_ui.advCtrlGroupBox.setVisible(False)
        super().show()
        self._on_scan_ports_button_clicked()
        if self.config.get('port') is not None:
            self._enable_ui_functions()
            self.bcb_if.connect(port=self.config.get('port'), baudrate=self.config.get('baudrate'))
        else:
            self._disable_ui_functions()
            self.main_window_ui.autoConnectCheckBox.setChecked(True)

    def shutdown(self):
        self.bcb_if.disconnect()
